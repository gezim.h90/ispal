var keystone = require('keystone');
var Types = keystone.Field.Types;


var Articles = new keystone.List('Articles', {
});

Articles.add({
  title: { type: String, required: true, default: ''},
  name: { type: Types.Name, required: true },
});

Articles.register()
