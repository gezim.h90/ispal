import Link from 'next/link'

const Topic = (props) => (
  <div>
    <Link href={ props.data.link }>
      <h5 className='topic' style={{background: props.data.background}}>
        {props.data.text}
      </h5>
    </Link>

    <style jsx>{`
        .topic {
          width: 100%;
          background: #007A3D;
          margin-bottom: 10px;
          padding: 20px 5px;
          color: white;
          font-weight: bold;
          cursor: pointer;
        }
      `}
    </style>
  </div>
)

export default Topic;
