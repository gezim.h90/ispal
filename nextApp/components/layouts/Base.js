import PropTypes from 'prop-types'
import indexStyle from '../../styles/index.scss'

import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';

import { cloudinary } from 'cloudinary-core'

import Link from 'next/link'
import Head from 'next/head'

class Base extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    return (
      <div>
        <CssBaseline />
        <Head>
          <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet"></link>
        </Head>
        <style dangerouslySetInnerHTML={{ __html: indexStyle }} />

        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>
          <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous"></link>
          <link href="https://fonts.googleapis.com/css?family=Yeseva+One" rel="stylesheet"></link>
          <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous"></link>
          <link rel='stylesheet' href='/icons/style.css'></link>
          <link href="https://fonts.googleapis.com/css?family=Fira+Sans" rel="stylesheet"></link>
          <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous"></link>
        </Head>

        <AppBar position="static" color="white" className='navbar2'>
          <Toolbar>
            <Typography variant="title" color="inherit" style={{ flex: 1 }}>
              <Link href='/'>
                <div className='logo'>
                  <span className='first'>Félagið</span><br/>
                  <span className='second'><span>Ísland</span>-<span>Palestína</span></span>
                </div>
              </Link>
            </Typography>

            <Button color="inherit">
              <Link href="/about">About</Link>
            </Button>
            <Button color="inherit">
              <Link href="/about">About</Link>
            </Button>
          </Toolbar>
        </AppBar>


        <div className='container'>
          {this.props.children}
        </div>


        <style jsx global>{`
          .navbar2 {
            background-color: white!important;
            border-bottom: 2px solid $green;
            margin-bottom: 30px;
            padding-top: 5px;
            padding-bottom: 15px;
          }

          .logo {
            font-family: 'Yeseva One', cursive;
            color: green;
            cursor: pointer;
          }

          .logo .first {
            color: black;
          }

          .logo .second {
            font-size: 1.6em;
            position: relative;
            top: 7px;
          }

        `}</style>

      </div>

    );
  }
}

export default Base;


// Base.propTypes = {
//     children: PropTypes.oneOfType([
//         PropTypes.arrayOf(PropTypes.element),
//         PropTypes.element
//     ]).isRequired,
// };
