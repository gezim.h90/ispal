import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import styles from '../styles/components/ArticleCard.scss'

import { Image, Transformation } from 'cloudinary-react';

import { Cloudinary } from 'cloudinary-core';


export class ArticleCard extends React.Component {

  constructor(props) {
    super(props)

    this.img = Cloudinary.new( { cloud_name: "ispal"});
  }

  render() {
    return (
      <div className='card'>
        <style dangerouslySetInnerHTML={{ __html: styles }} />

        <Card>
          <CardMedia
            className='card-media'
            image={ this.img.url(this.props.data.image.public_id, { width: 500, crop: 'scale' }) }
            title={ this.props.data.image_meta }
          />

          <CardContent>
            <Typography gutterBottom variant="headline" component="h2">
              { this.props.data.title }
            </Typography>
            <Typography component="p">
              { this.props.data.text }
            </Typography>
          </CardContent>

        </Card>

        <style jsx global>{`
          .card-media {
            height: 0;
            padding-top: 56.25%;
          }
        `}</style>

      </div>
    )
  }
}


export const ArticleCardList = (props) => (
  <div>

  </div>
);
