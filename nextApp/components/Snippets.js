
export const BoxedIconAndTitle = (props) => (
  <div className='box'>
    <i className={props.icon}></i>
    <h2>{props.text}</h2>

    <style jsx>{`
      .box {
        text-align: center;
        border: 2px solid;
        border-radius: 4px;
        cursor: pointer;
        padding: 10px;
      }
      .box i {
        font-size: 3em;
      }
    `}</style>
  </div>
);

export const Quote = (props) => (
  <div className='quote'>
    <i className='fas fa-quote-left'></i>
    <p>{props.text}</p>

    <style jsx>{`
        .quote {
          font-size: 1.8em;
        }
        .quote i {
          position: absolute;
          top: 0;
          left: 0px;
          opacity: 0.2;
        }
    `}</style>
  </div>
);
