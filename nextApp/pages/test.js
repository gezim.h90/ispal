import { Component } from 'react';
import axios from 'axios'

import {Image, CloudinaryContext, Transformation} from 'cloudinary-react';

class App extends Component {

  static async getInitialProps() {
    let response = await axios.get('http://localhost:3000/api/posts');
    return { posts: response.data };
  }

  render() {
    return (
      <div className='container'>
        <style jsx>{`
            .header {
              padding: 16px 16px;
            }
            .content {
              padding: 16px 16px;
            }
            .post {
              margin-bottom: 16px;
            }
        `}</style>
        <div className='header'>
          <h1>Keystone Next Example</h1>
        </div>
        <div className='content'>
          { this.props.posts.map((post, i) => {
              return (
                <div className='post' key={i}>
                  <div className='row'>
                    <div className='col-12 col-md-4'>
                      <CloudinaryContext cloudName="ispal">
                          <Image publicId='vfhbzwneo9m7uyrienbk'>
                              <Transformation width="200" crop="scale" angle="10"/>
                          </Image>
                      </CloudinaryContext>
                    </div>
                    <div className='col-12 col-md-8'>
                      <h2>{post.title}</h2>
                      <div dangerouslySetInnerHTML={{__html: post.content.brief}}></div>
                      <h1>Extended2</h1>
                      <div dangerouslySetInnerHTML={{__html: post.content.extended}}></div>
                    </div>
                  </div>
                </div>
              );
            }) }
        </div>
      </div>
    );
  };
}

export default App;
