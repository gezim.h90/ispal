import Base from '../components/layouts/Base'
import Survey from '../components/Survey'

const Quiz = () => (
  <Base>
    <Survey />
  </Base>
);

export default Quiz;
