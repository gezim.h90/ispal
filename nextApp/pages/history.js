import Base from '../components/layouts/Base'


const History = () => (
  <Base>
    <iframe src='https://interactive.aljazeera.com/aje/PalestineRemix/maps/vanishingpalestine/index.html' height='625' width='100%'></iframe>
  </Base>
);

export default History;
