import Link from 'next/link'
import fetch from 'isomorphic-unfetch'

import Base from '../components/layouts/Base'
import { ArticleCard } from '../components/ArticleCard'
import Topic from '../components/Topic'
import { BoxedIconAndTitle, Quote } from '../components/Snippets'

import CountUp from 'react-countup';

import Grid from '@material-ui/core/Grid';

const topics = [
  { text: 'Að ferðast um Palestínu'},
  { text: 'Sniðgangan'},
  { text: 'Saga Palestínu', link: '/history'},
  { text: 'Spurningaleikur', link: '/quiz'}
]

const articles = [
  {
    image: 'https://www.foreignpolicyjournal.com/wp-content/uploads/2018/02/palestine-flag.jpg',
    title: 'Söngvakeppnir og samviska',
    text: 'Árið er 1973. Vor er í lofti í Lúx­em­borg þar sem halda á söngvakeppni evr­ópskra sjón­varps­stöðva og bjóða nýja keppn­is­þjóð vel­komna. Nei, það land til­heyrir ekki Evr­ópu en fellur land­fræði­lega innan útvörpun­ar­svæðis heims­álf­unnar eins og Sam­band evr­ópskra sjón­varps­stöðva skil­greinir það.',
  }
]

const events = [
  {
    image: 'https://static01.nyt.com/images/2017/02/21/dining/21COOKING-HUMMUS1/21COOKING-HUMMUS1-articleLarge.jpg',
    title: 'Samstöðufundur með Palestínu'
  },
  {
    image: 'https://www.elsevier.com/__data/assets/image/0018/30762/Doa-Hawamdeh.jpg',
    title: 'Aisha samtökin styrkt um 50.000 dollara'
  },
  {
    image: 'https://i.ytimg.com/vi/UpVsgeuPNjQ/maxresdefault.jpg',
    title: 'Speed sisters sýnd í bíó'
  },
  {
    image: 'http://nena-news.it/wp-content/uploads/2014/08/Norvegesi.jpg',
    title: 'Mads Gilbert heldur fyrirlestur'
  }
]

const culture = [
  {
    image: 'http://gopalestine.org/wp-content/uploads/2016/10/1-1.jpg',
    title: 'Matseðlar'
  },
  {
    image: 'http://mediad.publicbroadcasting.net/p/wfae/files/styles/x_large/public/201309/1_WeddingHenna.JPG',
    title: 'Menning'
  },
  {
    image: '',
    title: ''
  },
  {
    image: '',
    title: ''
  },
]

const items = [
  {
    src: 'https://scontent-ams3-1.xx.fbcdn.net/v/t1.0-9/10441133_10152504727762252_8775663234183711629_n.jpg?_nc_cat=0&oh=c33ca2692cba95fb593abc48169776a7&oe=5B7E9026',
    altText: 'Slide 1',
    caption: 'Slide 1'
  },
  {
    src: 'https://scontent-ams3-1.xx.fbcdn.net/v/t1.0-9/1509737_10152502913142252_1558945261978886520_n.jpg?_nc_cat=0&oh=aad5e703b9931a480059f2314353b4e4&oe=5BBB8327',
    altText: 'Slide 2',
    caption: 'Slide 2'
  },
  {
    src: 'https://scontent-ams3-1.xx.fbcdn.net/v/t1.0-9/10734054_10152502913017252_6538745780988569674_n.jpg?_nc_cat=0&oh=a6d80b85b381739cfa9544c7b6d391f1&oe=5BBF16C3',
    altText: 'Slide 3',
    caption: 'Slide 3'
  },
  {
    src: 'https://scontent-ams3-1.xx.fbcdn.net/v/t1.0-9/10689882_10152505384807252_5139845613804160893_n.jpg?_nc_cat=0&oh=bc158657a2f80fc4da013ba430936cbe&oe=5B77E7E5',
    altText: 'Slide 3',
    caption: 'Slide 3'
  },
];

class Index extends React.Component {

  static async getInitialProps() {
    const res = await fetch('http://localhost:3000/api/posts')
    const data = await res.json()

    return {
      posts: data
    }
  }

  constructor(props) {
    super(props);
    this.state = { activeIndex: 0 };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }

  onExiting() {
     this.animating = true;
   }

   onExited() {
     this.animating = false;
   }

   next() {
     if (this.animating) return;
     const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
     this.setState({ activeIndex: nextIndex });
   }

   previous() {
     if (this.animating) return;
     const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
     this.setState({ activeIndex: nextIndex });
   }

  goToIndex(newIndex) {
   if (this.animating) return;
   this.setState({ activeIndex: newIndex });
  }

  render() {
    const { activeIndex } = this.state;
    const posts = this.posts;

    return (
      <div>
        <Base>

          <Grid container spacing={32}>
            <Grid item xs={12} md={6}>
              <div className='col-block'>
                <ArticleCard data={ articles[0] }></ArticleCard>
              </div>
            </Grid>

            <Grid item xs={12} md={3}>
              <div className='col-block'>
                {
                  topics.map(function(topic, i) {
                    return (
                      <Topic data={topic}></Topic>
                    )
                  })
                }
                <Grid container spacing={16}>
                  <Grid item xs={6}>
                    <BoxedIconAndTitle text='Ganga í félagið' icon='lnr lnr-users'></BoxedIconAndTitle>
                  </Grid>
                  <Grid item xs={6}>
                    <BoxedIconAndTitle text='Sækja um styrk' icon='lnr lnr-heart'></BoxedIconAndTitle>
                  </Grid>
                </Grid>
              </div>
            </Grid>

            <Grid item xs={12} md={3}>
              <div className='col-block'>
                <h1 className='text-center no-margin mb-20'>Um hvað snýst baráttan?</h1>
                <div className='align-center objectives'>
                  <Grid container>
                    <Grid item xs={6}><h1><CountUp start={0} end={8000} duration='1'/></h1> <h4>Börn í fangelsum</h4></Grid>
                    <Grid item xs={6}><h1><CountUp start={0} end={500000} separator='.' duration='1'/></h1> <h4>Fá ekki að snúa heim</h4></Grid>
                    <Grid item xs={6}><h1><CountUp start={0} end={70}></CountUp> Ár</h1><p>Undir hernámi og aðskilanarðstefnu</p></Grid>
                    <Grid item xs={6}><h1><CountUp start={0}></CountUp></h1></Grid>
                    <Grid item xs={12}><Quote text='90% af vatninu ódrykkjanlegt'></Quote></Grid>
                    <Grid item xs={12}>
                      <p className='align-left'>
                        Félagið Ísland-Palestína var stofnað 29. nóvember 1987. Markmið félagsins var frá upphafi að stuðla að jákvæðum viðhorfum til ísraelsku og palestínsku þjóðanna og vinna gegn hvers kyns aðskilnaðarstefnu. Félagið hefur stutt baráttu Palestínumanna gegn hernámi og rétt flóttafólks til að snúa heim. Þann 18. maí 1989 náðist samstaða á Alþingi um ályktun sem fól í sér stuðning við öll meginmarkmið félagsins, tilverurrétt Ísraelsríkis og þjóðarréttindi Palestínumanna. Hér má lesa lög félagsins og hér má lesa yfirlit yfir starfsemi félagsins.
                      </p>
                    </Grid>
                  </Grid>
                </div>
              </div>
            </Grid>
          </Grid>



          <div className='col-block mt-20'>
            <h5 className='boxed-title no-margin mb-20'>Viðburðir</h5>
            <Grid container spacing={32}>
              {
                this.props.posts.map(function(event, i) {
                  return (
                    <Grid item md={3}>
                      <ArticleCard data={event}></ArticleCard>
                    </Grid>
                  )
                })
              }
            </Grid>
          </div>


          <div className='col-block mt-20'>
            <h5 className='boxed-title no-margin mb-20'>Menning</h5>
            <Grid container spacing={32}>
              {
                culture.map(function(item, i) {
                  return (
                    <Grid item md={3}>
                      <ArticleCard data={item}></ArticleCard>
                    </Grid>
                  );s
                })
              }
            </Grid>
          </div>


          <div className='col-block mt-20'>
            <h5 className='boxed-title no-margin mb-20'>Myndir</h5>
            <Grid container>
            </Grid>
          </div>



        </Base>

        <style jsx>{`
          .col-block {
            margin: 0px -10px;
            background: white;
            height: 100%;
            padding: 10px;
            box-shadow: 0px 0px 10px -5px black;
          }

          .objectives h1 {
            font-size: 3em;
            margin: 0;
            margin-bottom: 15px;
          }

          .objectives .row {
            margin-bottom: 20px;
          }

          `}</style>
        </div>
    ); // return
  } // render
} // component

export default Index;
