// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
require('dotenv').config();

// Next app
const next = require('next');
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev, dir: './nextApp' });

// Require keystone
var keystone = require('keystone');

// Initialise Keystone with your project's configuration.
// See http://keystonejs.com/guide/config for available options
// and documentation.

keystone.init({
	'name': 'ispal',
	'brand': 'ispal',

	'wysiwyg cloudinary images': true,

	'wysiwyg additional plugins': 'image autolink autoresize advlist fullscreen media autosave wordcount preview imagetools',

	'wysiwyg additional options': {
		menubar: true,
		image_list: [
			{title: 'Image 1', value: 'http://res.cloudinary.com/ispal/image/upload/vfhbzwneo9m7uyrienbk'},
			{title: 'Image 2', value: 'http://res.cloudinary.com/ispal/image/upload/v1528068444/auynrhgat4h9ndblqojy.png'}
		],
		external_image_list: [
			{title: 'Image 1', value: 'http://res.cloudinary.com/ispal/image/upload/vfhbzwneo9m7uyrienbk'},
			{title: 'Image 2', value: 'http://res.cloudinary.com/ispal/image/upload/v1528068444/auynrhgat4h9ndblqojy.png'}
		],
		content_css: '/styles/tinymce.css'
	},

	'auto update': true,
	'session': true,
	'auth': true,
	'user model': 'User',

	'static': 'public'
});

// Load your project's Models
keystone.import('models');

// Start Next app
app.prepare()
	.then(() => {

		// Load your project's Routes
		keystone.set('routes', require('./routes')(app));

		// Configure the navigation bar in Keystone's Admin UI
		keystone.set('nav', {
			posts: ['posts'],
			users: 'users',
		});

		keystone.start();
	});
